import {
  createBoard,
  deleteBoard,
  getBoards,
  getListFromBoard,
  getCardFromList,
  createCard,
  createList,
  deleteList,
  deleteCard,
} from "./apiCall.js";

// targeting elements
let create_form = document.querySelector(".createBoard");
let addBoard = document.querySelector(".create-board");
let createBoardDialog = document.querySelector(".createBoardDialog");
let boardArea = document.querySelector("board-area");
let details = document.querySelector(".boardDetails");
let secondPage = document.querySelector(".second-page");
let listArea = document.querySelector(".list-area");
let listAddBtn = document.querySelector(".listAddBtn");
let addListDialog = document.querySelector(".addListDialog");
let homePageBoardContainer = document.querySelector(".homePageBoardContainer");

// html Templates
function listHtml(listData) {
  return ` <div class="flex justify-between  items-center">
  <p>${listData.name}</p>
  <i class="fa-solid fa-ellipsis listAction"></i>
</div>
<div class="addListDialog hidden absolute left-52 z-10">
      <div class="flex justify-between  items-center ">
          <h1 class="font-bold ">List Actions  </h1>
          <i class="fa-solid fa-xmark listClose"></i>
      </div>
      <p class="deleteList" data-list-id="${listData.id}">Archive this List</p>
</div>
 

<div class="cards-area" id="${listData.id}">
</div>
<div class="hidden">
  <p class="text-red-500 hidden"> Enter the card Name </p>
  <input class="input-card " type="text" placeholder="Enter a title for this card" required>
  <button data-list-id="${listData.id}" class="bg-blue-600 w-max px-3 button">Add Card </button>
  &nbsp &nbsp  <i class="fa-solid fa-xmark cardClose"></i>  
</div>
<p class="add-card">
  <i class="fa-solid fa-plus"></i> add a card
</p> `;
}

function cardHtml(cardData) {
  return `<p class="cardDisplay">${cardData.name} </p>
     <div class="listData hidden border  absolute left-52 z-10">  
      <div class="flex justify-between items-center">
          <p>${cardData.name}</p>
          <i class="fa-solid fa-xmark close"></i>
         
      </div>
      <p class="deleteCard" data-card-id=${cardData.id}>Archive this card</p>
  </div>`;
}

function updateBoardNamesHtml(boardData) {
  return `<div class="flex justify-between items-center boardNameTagContainer">
            <p class="boardNameTag" data-board-id=${boardData.id} >${boardData.name} </p>
            <i class=" hidden  fa-solid fa-ellipsis board-options"></i>
         </div>
     <div class="listData hidden border  absolute left-32 z-10">  
      <div class="flex justify-between items-center">
          <p>${boardData.name}</p>
          <i class="fa-solid fa-xmark close"></i>
      </div>
      <p class="delete-Board" data-board-id=${boardData.id}>Archive this board</p>
  </div>`;
}

document.addEventListener("DOMContentLoaded", () => {
  getBoards("662f7b52db72821c6fa9c59e")
    .then((data) => {
      data.forEach((element) => {
        if (!element.closed) {
          updateHomePageBoardsUI(element);
          updateBoardNamesUI(element);
        }
      });
    })
    .catch((err) => {
      console.log(err);
    });
});

function cardUI(cardData) {
  let container = document.createElement("div");
  let cardBox = cardHtml(cardData);
  let cardArea = document.getElementById(`${cardData.idList}`);

  container.innerHTML = cardBox;
  cardArea.append(container);
}
function deleteListInUI(listId) {
  deleteList(listId).then((data) => {
    console.log(data);
  });
}

function createBoardInUI(boardName) {
  createBoard(boardName)
    .then((res) => {
      console.log(res);
      updateHomePageBoardsUI(res);
      updateBoardNamesUI(res);
    })
    .catch((err) => {
      console.log(err);
    });
}

function getBoardIdFromUI(id) {
  getListFromBoard(id).then((lists) => {
    lists.forEach((element) => {
      listDataUI(element);
    });
  });
}

function listDataUI(listData) {
  let newList = document.createElement("div");
  newList.setAttribute("class", "listData");
  newList.style.position = "relative";
  newList.innerHTML = listHtml(listData);
  listAddBtn.setAttribute("data-board-id", listData.idBoard);
  getCardFromList(listData.id).then((cardData) => {
    cardData.forEach((element) => {
      cardUI(element);
    });
  });
  listArea.append(newList);
}

addBoard.addEventListener("click", (event) => {
  if (event.target.classList.contains("create-board")) {
    createBoardDialog.classList.toggle("hidden");
  }
});

function updateHomePageBoardsUI(boardsData) {
  const colors = ["red", "blue", "green", "yellow", "purple", "orange", "gray", "pink", "brown", "aqua"];
  const randomIndex = Math.floor(Math.random() * colors.length);
  const randomColor = colors[randomIndex];
  let newBoard = document.createElement("p");
  newBoard.setAttribute("class", "board");
  newBoard.setAttribute("id", boardsData.id);
  newBoard.style.backgroundColor = randomColor;
  newBoard.innerText = boardsData.name;
  homePageBoardContainer.lastElementChild.lastElementChild.before(newBoard);
}

homePageBoardContainer.addEventListener("click", (event) => {
  if (event.target.classList.contains("closeForm")) {
    event.target.parentElement.parentElement.style.display = "none";
  }
});

create_form.addEventListener("submit", (event) => {
  event.preventDefault();
  createBoard(create_form.children[1].value)
    .then((res) => {
      console.log(res);
      updateHomePageBoardsUI(res);
      event.target.parentElement.style.display = "none";
      create_form.children[1].value = "";
    })
    .catch((er) => {
      console.log(err);
    });
});

// secondPage
homePageBoardContainer.addEventListener("click", (event) => {
  if (event.target.classList.contains("board")) {
    secondPage.style.display = "flex";
    homePageBoardContainer.style.display = "none";
    getBoardIdFromUI(event.target.getAttribute("id"));
  }
});

function updateBoardNamesUI(boardData) {
  let boardNameCon = document.createElement("div");
  boardNameCon.setAttribute("class", "boardNameContainer");
  let a = updateBoardNamesHtml(boardData);
  boardNameCon.innerHTML = a;
  details.append(boardNameCon);
}

details.addEventListener("click", (event) => {
  if (event.target.classList.contains("boardNameTag")) {
    listArea.innerHTML = "";
    let id = event.target.getAttribute("data-board-id");
    getBoardIdFromUI(id);
  } else if (event.target.classList.contains("clickMe")) {
    event.target.parentElement.nextElementSibling.style.display = "block";
  } else if (event.target.classList.contains("close")) {
    event.target.parentElement.parentElement.style.display = "none";
  } else if (event.target.classList.contains("createBtn")) {
    event.preventDefault();
    let boardName = event.target.previousElementSibling.value;
    createBoardInUI(boardName);
    event.target.parentElement.parentElement.style.display = "none";
    event.target.previousElementSibling.value = " ";
  } else if (event.target.classList.contains("board-options")) {
    event.target.parentElement.nextElementSibling.style.display = "block";
  } else if (event.target.classList.contains("delete-Board")) {
    let boardId = event.target.getAttribute("data-board-id");
    deleteBoard(boardId).then(console.log);
    event.target.parentElement.parentElement.style.display = "none";
  }
});


listArea.addEventListener("click", (event) => {
  if (event.target.classList.contains("add-card")) {
    event.target.previousElementSibling.style.display = "block";
    event.target.style.display = "none";
  } else if (event.target.classList.contains("cardClose")) {
    event.target.parentElement.style.display = "none";
    event.target.parentElement.nextElementSibling.style.display = "block";
  } 
  else if (event.target.classList.contains("button")) {
    let cardName = event.target.parentElement.children[1].value;
    if (cardName) {
      let listId = event.target.getAttribute("data-list-id");
       createCard(listId, cardName).then((res) => {
        cardUI(res);
        event.target.parentElement.children[1].value = "";
        event.target.previousElementSibling.previousElementSibling.style.display =
          "none";
      });
    } else {
      event.target.previousElementSibling.previousElementSibling.style.display =
        "block";
    }
  } else if (event.target.classList.contains("listAction")) {
    event.target.parentElement.nextElementSibling.classList.toggle("hidden");
  } else if (event.target.classList.contains("deleteList")) {
    let listId = event.target.getAttribute("data-list-id");
    deleteListInUI(listId);
    event.target.parentElement.parentElement.style.display = "none";
  } else if (event.target.classList.contains("cardDisplay")) {
    event.target.parentElement.children[1].style.display = "block";
  } else if (event.target.classList.contains("close")) {
    event.target.parentElement.parentElement.style.display = "none";
  } else if (event.target.classList.contains("deleteCard")) {
    let cardId = event.target.getAttribute("data-card-id");
    deleteCard(cardId).then(console.log);
    event.target.parentElement.previousElementSibling.style.display = "none";
    event.target.parentElement.style.display = "none";
  }else if(event.target.classList.contains("listClose")){
    event.target.parentElement.parentElement.classList.add("hidden")
  }
});


addListDialog.addEventListener("click", (event) => {
  if (event.target.classList.contains("addList")) {
    event.target.previousElementSibling.style.display = "block";
    event.target.style.display = "none";
  } else if (event.target.classList.contains("cardAddClose")) {
    event.target.parentElement.style.display = "none";
    event.target.parentElement.nextElementSibling.style.display = "block";
  } else if (event.target.classList.contains("listAddBtn")) {
    console.log("listAddBtn");
    let listName = event.target.parentElement.children[0].value;
    let idBoard = event.target.getAttribute("data-board-id");
    createList(idBoard, listName).then((res) => {
      listDataUI(res);
    });
    event.target.parentElement.children[0].value = ""
  }
});
