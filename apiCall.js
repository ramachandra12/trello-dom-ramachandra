const APIToken =
  "ATTA3b4afa91e29b410cdb9b58015ba52d7e5bea261bd6505960f5991fb019282247A3A85651";
const APIKey = "7d5923b2fb49db8f1a0d7ace94e5dc29";

function createBoard(name) {
  return fetch(
    `https://api.trello.com/1/boards/?name=${name}&key=${APIKey}&token=${APIToken}`,
    {
      method: "POST",
    }
  ).then((response) => {
    return response.json();
  });
}

function deleteBoard(id) {
  return fetch(
    `https://api.trello.com/1/boards/${id}/closed?key=${APIKey}&token=${APIToken}&value=true`,
    {
      method: "PUT",
    }
  ).then((response) => {
    return response.json();
  });
}

function getBoards(id) {
  return fetch(
    `https://api.trello.com/1/members/${id}/boards?key=${APIKey}&token=${APIToken}`,
    {
      method: "GET",
    }
  ).then((response) => {
    return response.json();
  });
}

function getMembershipId(id) {
  return fetch(
    `https://api.trello.com/1/boards/${id}/memberships?key=${APIKey}&token=${APIToken}`,
    {
      method: "GET",
    }
  ).then((response) => {
    return response.json();
  });
}

function getListFromBoard(id) {
  return fetch(
    `https://api.trello.com/1/boards/${id}/lists?key=${APIKey}&token=${APIToken}`,
    {
      method: "GET",
    }
  ).then((response) => {
    return response.json();
  });
}

function getCardFromList(id) {
  return fetch(
    `https://api.trello.com/1/lists/${id}/cards?key=${APIKey}&token=${APIToken}`,
    {
      method: "GET",
    }
  ).then((response) => {
    return response.json();
  });
}

function createCard(id, name) {
  return fetch(
    `https://api.trello.com/1/cards?idList=${id}&name=${name}&key=${APIKey}&token=${APIToken}`,
    {
      method: "POST",
    }
  ).then((response) => {
    return response.json();
  });
}

function createList(id, name) {
  return fetch(
    `https://api.trello.com/1/lists?idBoard=${id}&name=${name}&key=${APIKey}&token=${APIToken}`,
    {
      method: "POST",
    }
  ).then((response) => {
    return response.json();
  });
}

function deleteList(listId) {
  return fetch(
    `https://api.trello.com/1/lists/${listId}/closed?key=${APIKey}&token=${APIToken}&value=true`,
    {
      method: "PUT",
    }
  ).then((response) => {
    return response.json();
  });
}

function deleteCard(cardId) {
  return fetch(
    `https://api.trello.com/1/cards/${cardId}/closed?key=${APIKey}&token=${APIToken}&value=true`,
    {
      method: "PUT",
    }
  ).then((response) => {
    return response.json();
  });
}

export {
  createBoard,
  deleteBoard,
  getMembershipId,
  getBoards,
  getListFromBoard,
  getCardFromList,
  createCard,
  createList,
  deleteList,
  deleteCard,
};
